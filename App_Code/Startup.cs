﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: Startup.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(OnlineCPS.Startup))]

namespace OnlineCPS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}