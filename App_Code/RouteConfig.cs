﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: RouteConfig.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace OnlineCPS
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);
        }
    }
}