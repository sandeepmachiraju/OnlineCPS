﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: StudentDatabaseConnections.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for StudentDatabaseConnections
/// </summary>
public class StudentDatabaseConnections
{
    SqlConnection conn;
    string conStr = ConfigurationManager.ConnectionStrings["OnlineCPS"].ConnectionString;

    public StudentDatabaseConnections()
    {
    }

    //
    // TODO: Add constructor logic here
    //
    public String getLoginStatus(String IdentificationNumber, String Password, String User)
    {
        String message = "Error in Connecting";
        try
        {
            conn = new SqlConnection(conStr);
            if (User == "Student")
            {
                string query = "Select SId,SPassword from dbo.StudentLoginDetails";
                SqlCommand command = new SqlCommand(query, conn);
                command.Notification = null;
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.HasRows)
                {
                    reader.Read();

                    if (String.Equals(IdentificationNumber, reader[0].ToString()) &&
                        String.Equals(Password, reader[1].ToString()))
                    {
                        message = "Success";
                        conn.Close();
                        return message;
                    }
                    else
                    {
                        message = "Wrong EmailId or password. Please retry again";
                    }
                } //end of while
            } // end of Student User If
            else if (User == "FacultyAdvisor")
            {
                string query = "Select FId,FPassword from dbo.FacultyLoginDetails";
                SqlCommand command = new SqlCommand(query, conn);
                command.Notification = null;
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.HasRows)
                {
                    reader.Read();

                    if (String.Equals(IdentificationNumber, reader[0].ToString()) &&
                        String.Equals(Password, reader[1].ToString()))
                    {
                        message = "Success";
                        conn.Close();
                        return message;
                    }
                    else
                    {
                        message = "Wrong EmailId or password. Please retry again";
                    }
                } //end of while
            }
            else if (User == "AcademicAdvisor")
            {
            }
            else
            {
                // Exception no raduio button checked
                conn.Close();
            }
        } // end of try
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getLoginStatus()", ex);
            conn.Close();
        } // end of catch

        conn.Close();
        return message;
    } // end of getLoginStatus

    // Function that connects to datbase to get foundation courses
    public DataTable getStudentFoundationCourses(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentFoundationCourses =
                "select FCourseName, Bid, FCourseID,FGrade from dbo.StudentAssignedFoundationCourses where SId='" +
                SId + "'";
            SqlCommand command = new SqlCommand(getStudentFoundationCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentFoundationCourses()", ex);
            conn.Close();
        }

        return dt;
    }

    // Function that connects to database to get Student starting year and semester
    public DataTable getStudentYearSemester(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentYearSemester =
                "select StartingSemester, StartingYear from dbo.StudentInformation where SId='" + SId + "'";
            SqlCommand command = new SqlCommand(getStudentYearSemester, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentYearSemester()", ex);
            conn.Close();
        }

        return dt;
    }

    // Function that connects to database to get Universirty Semester List
    public DataTable getUniversitySemesterList(String SemesterStart)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getUniversitySemesterList = "select SemId,Semester from dbo.UniversitySemesters";
            SqlCommand command = new SqlCommand(getUniversitySemesterList, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getUniversitySemesterList()", ex);
            conn.Close();
        }

        return dt;
    }

    /* Function that connects to Database to get Student Core Courses List*/
    public DataTable getStudentCoreCourses(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentFoundationCourses =
                "select BId,CCourseId, CCourseName, CCreditHours from dbo.BranchCoreCourses where BId= (select BId from dbo.StudentLoginDetails where SId= '" +
                SId + "')";
            SqlCommand command = new SqlCommand(getStudentFoundationCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentCoreCourses()", ex);
            conn.Close();
        }

        return dt;
    }

    /* Function that connects to Database to get Student Elective Courses List*/

    public DataTable getStudentElectiveCourses(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            String getStudentElectiveCourses =
                "select x.BId2, y.ECourseId, y.ECourseName, y.ECreditHours,x.NoOfElectives from dbo.Branch2BranchElectiveCount x, dbo.StudentLoginDetails z, dbo.BranchElectiveCourses y where z.SId = '" +
                SId + "' and z.BId = x.BId1 and x.BId2 = y.BId";
            SqlCommand command = new SqlCommand(getStudentElectiveCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentElectiveCourses()", ex);
            conn.Close();
        }

        return dt;
    }

    /* Function that connects to Database to get no of electives to be displayed based upon selected course option by student*/

    public DataTable getStudentNoOfElective(String SId, String COption)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            String getStudentElectiveCourses =
                "select x.BId,y.NoOfElectiveCreditHrs,z.BCourseOptionHours from dbo.StudentLoginDetails x,dbo.BranchInformation y, dbo.MajorCourseOptions z where x.SId='" +
                SId + "' and x.BId = y.BId and y.BId = z.BId and z.BCourseOption = '" + COption + "'";
            SqlCommand command = new SqlCommand(getStudentElectiveCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentNoOfElective()", ex);
            conn.Close();
        }

        return dt;
    }


    /*********************************************************************************************************/
    /*Setting Student Request Details*/
    public String setStudentRequestDetails(DataTable details)
    {
        conn = new SqlConnection(conStr);
        conn.Open();
        String message = "Failure";
        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentCpsRequestDetails";
            //bulkCopy.ColumnMappings.Add("SId", "SId");
            //bulkCopy.ColumnMappings.Add("RequestId", "RequestId");
            //bulkCopy.ColumnMappings.Add("RequestDate", "RequestDate");
            //bulkCopy.ColumnMappings.Add("RequestStatus", "RequestStatus");
            try
            {
                bulkCopy.WriteToServer(details);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentRequestDetails()", ex);
                message = "Failure";
                conn.Close();
            }
        }


        return message;
    }

    /*Setting student foundation courses*/
    public String setStudentFoundationCourses(DataTable fdt)
    {
        conn = new SqlConnection(conStr);
        conn.Open();
        String message = "Failure";
        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentFoundationCourses";
            try
            {
                bulkCopy.WriteToServer(fdt);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentFoundationCourses()", ex);
                message = "Failure";
                conn.Close();
            }

            return message;
        }
    }


    /*Setting student Core Courses*/
    public String setStudentCoreCourses(DataTable cdt)
    {
        conn = new SqlConnection(conStr);
        conn.Open();
        String message = "Failure";

        /*

        foreach (DataRow row in cdt.Rows)
        {
            Debug.WriteLine("");
            for (int x = 0; x < cdt.Columns.Count; x++)
            {
                Debug.Write(row[x].ToString() + " ");
            }
        }


        */


        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentCoreCourses";
            try
            {
                bulkCopy.WriteToServer(cdt);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentCoreCourses()", ex);
                message = "Failure";
                conn.Close();
            }

            return message;
        }
    }

    /*Setting student Elective Courses*/
    public String setStudentElectiveCourses(DataTable edt)
    {
        conn = new SqlConnection(conStr);
        conn.Open();
        String message = "Failure";
        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentElectiveCourses";
            try
            {
                bulkCopy.WriteToServer(edt);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentElectiveCourses()", ex);
                message = "Failure";
                conn.Close();
            }

            return message;
        }
    }

    /*Setting student coursework Course */
    public String setStudentCourseWorkCourse(DataTable cwdt)
    {
        conn = new SqlConnection(conStr);
        String message = "Failure";
        conn.Open();
        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentCourseWorkCourse";
            try
            {
                bulkCopy.WriteToServer(cwdt);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentCourseWorkCourse()", ex);
                message = "Failure";
                conn.Close();
            }

            return message;
        }
    }

    /*********************************************************************************************************/
    /*deleting CPS Request for inserting the record (means updating)*/
    public String deleteStudentCpsRequest(DataTable details)
    {
        String message = "Failure";
        try
        {
            conn = new SqlConnection(conStr);
            int deleteStatus;
            String deleteStudentCpsRequest = "delete from dbo.StudentCpsRequestDetails where SId = '" +
                                             details.Rows[0]["SId"] + "' and RequestId ='" +
                                             details.Rows[0]["RequestId"] + "'";
            SqlCommand command = new SqlCommand(deleteStudentCpsRequest, conn);
            conn.Open();
            deleteStatus = command.ExecuteNonQuery();
            if (deleteStatus > 0)
            {
                message = "Success";
                conn.Close();
                return message;
            }
            else
            {
                return message;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: updateStudentCpsRequest()", ex);
            conn.Close();
        }

        return message;
    }


    /*********************************************************************************************************/
    /*get submissionhistory details */

    public DataTable getStudentSubmissionHistory(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentFoundationCourses =
                "select RequestId, RequestDate, RequestStatus from dbo.StudentCpsRequestDetails where SId='" + SId +
                "'";
            SqlCommand command = new SqlCommand(getStudentFoundationCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSubmissionHistory()", ex);
            conn.Close();
        }

        return dt;
    }

    /*Deleting submission History*/
    public String deleteStudentSubmissionHistory(String RId, String SId)
    {
        String message = "Failure";
        try
        {
            conn = new SqlConnection(conStr);
            string deleteCpsRequestDetails = "delete from dbo.StudentCpsRequestDetails where SId='" + SId +
                                             "' and RequestId='" + RId + "'";
            SqlCommand command = new SqlCommand(deleteCpsRequestDetails, conn);
            conn.Open();
            int count = command.ExecuteNonQuery();

            if (count > 0)
            {
                message = "Success";
                conn.Close();
                return message;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: deleteStudentSubmissionHistory()", ex);
            conn.Close();
            return message;
        }
    }


    /* get Submission count, because if there are any previous submissions then student need to edit those submitted one's */

    public int getStudentSubmissionCount(String SId)
    {
        int count;
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentSubmissionCount =
                "select count(*) from dbo.StudentCpsRequestDetails where SId='" + SId + "'";
            SqlCommand command = new SqlCommand(getStudentSubmissionCount, conn);
            conn.Open();
            count = int.Parse(command.ExecuteScalar().ToString());
            conn.Close();
            return count;
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSubmissionCount()", ex);
            count = -1;
            conn.Close();
            return count;
        }
    }

    /*********************************************************************************************************/

    /* get Student Request Details (RId)*/
    public String getLatestRequestId(String SId)
    {
        String RId;
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentFoundationCourses =
                "select  TOP 1 RequestId from dbo.StudentCpsRequestDetails where SId='" + SId +
                "' ORDER BY RequestDate DESC";
            SqlCommand command = new SqlCommand(getStudentFoundationCourses, conn);
            conn.Open();

            RId = command.ExecuteScalar().ToString();
            if (RId != null)
            {
                conn.Close();
                return RId;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSubmissionHistory()", ex);
            conn.Close();
        }

        return "Failure";
    }

    /* get Student Request Foundation Courses*/
    public DataTable getStudentSelectedFoundationCourses(String SId, String RId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentSelectedFoundationCourses =
                "select sfc.FSemester, sfc.FCourseId, sfc.FGrade,safc.FCourseName from dbo.StudentFoundationCourses sfc,dbo.StudentAssignedFoundationCourses safc where safc.SId= '" +
                SId + "' and sfc.RequestId='" + RId + "' and sfc.FCourseId = safc.BId +' '+safc.FCourseId";
            SqlCommand command = new SqlCommand(getStudentSelectedFoundationCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSelectedFoundationCourses()",
                ex);
            conn.Close();
        }

        return dt;
    }

    /* get Student Request Core Courses*/
    public DataTable getStudentSelectedCoreCourses(String SId, String RId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentSelectedCoreCourses =
                "select scc.CSemester, scc.CCourseId, scc.CGrade,bcc.CCourseName from dbo.StudentCoreCourses scc,dbo.BranchCoreCourses bcc where scc.CCourseId = bcc.BId + ' '+bcc.CCourseId and scc.RequestId= '" +
                RId + "'";
            SqlCommand command = new SqlCommand(getStudentSelectedCoreCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSelectedCoreCourses()", ex);
            conn.Close();
        }

        return dt;
    }

    /* get Student Request Elective Courses*/
    public DataTable getStudentSelectedElectiveCourses(String SId, String RId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentSelectedElectiveCourses =
                "select sec.ESemester, sec.ECourseId, sec.EGrade,bec.ECourseName from dbo.StudentElectiveCourses sec,dbo.BranchElectiveCourses bec where sec.ECourseId = bec.BId + ' '+bec.ECourseId and sec.RequestId= '" +
                RId + "'";
            SqlCommand command = new SqlCommand(getStudentSelectedElectiveCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSelectedElectiveCourses()",
                ex);
            conn.Close();
        }

        return dt;
    }


    /* get Student Request CourseWork Courses*/
    public DataTable getStudentSelectedCourseWorkCourses(String SId, String RId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentSelectedCourseWorkCourses =
                "select scwc.CwCourseName, scwc.CwGrade from dbo.StudentCourseWorkCourse scwc where scwc.RequestId= '" +
                RId + "'";
            SqlCommand command = new SqlCommand(getStudentSelectedCourseWorkCourses, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentSelectedCourseWorkCourses()",
                ex);
            conn.Close();
        }

        return dt;
    }

    /* get Status of Request*/

    public String getStudentRequestStatus(String SId, String RId)
    {
        String Status = null;
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentFoundationCourses = "select  RequestStatus from dbo.StudentCpsRequestDetails where SId='" +
                                                 SId + "' and RequestId ='" + RId + "'";
            SqlCommand command = new SqlCommand(getStudentFoundationCourses, conn);
            conn.Open();

            Status = command.ExecuteScalar().ToString();
            if (Status != null)
            {
                conn.Close();
                return Status;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentRequestStatus()", ex);
            conn.Close();
        }

        return "Failure";
    }

    /*********************************************************************************************************/
    // get student appointment details
    public DataTable getStudentSubmissionAppointmentDetails(String SId, String RId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            String getStudentSubmissionAppointmentDetails =
                "Select AppointmentDate from StudentAppointmentDetails where SId ='" + SId + "' and RId='" + RId + "'";
            SqlCommand command = new SqlCommand(getStudentSubmissionAppointmentDetails, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                return dt;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(
                "Exception in Connecting to OnlineCPS database::: getStudentSubmissionAppointmentDetails()", ex);
            conn.Close();
        }

        return dt;
    }

    public String setAppointment(DataTable AppointmentDetails)
    {
        conn = new SqlConnection(conStr);
        conn.Open();
        String message = "Failure";
        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        {
            bulkCopy.DestinationTableName = "dbo.StudentAppointmentDetails";
            try
            {
                bulkCopy.WriteToServer(AppointmentDetails);
                message = "Success";
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in Connecting to OnlineCPS database::: setStudentElectiveCourses()", ex);
                message = "Failure";
                conn.Close();
            }

            return message;
        }
    }

    // get Faculty Id using Student Id

    public String getFacultyRelatedtoStudent(String SId)
    {
        String FId = null;
        try
        {
            conn = new SqlConnection(conStr);
            string getFacultyRelatedtoStudent = "select  FId from dbo.StudentFaculty where SId='" + SId + "'";
            SqlCommand command = new SqlCommand(getFacultyRelatedtoStudent, conn);
            conn.Open();

            FId = command.ExecuteScalar().ToString();
            if (FId != null)
            {
                conn.Close();
                return FId;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getFacultyRelatedtoStudent()", ex);
            conn.Close();
        }

        return FId;
    }


    // get Apppointment history details

    public DataTable getStudentAppointmentHistory(String SId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentAppointmentHistory =
                "select RId, AppointmentDate from dbo.StudentAppointmentDetails where SId='" + SId + "'";
            SqlCommand command = new SqlCommand(getStudentAppointmentHistory, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentAppointmentHistory()", ex);
            conn.Close();
        }

        return dt;
    }

    //delete Appointment details using RequestId
    public String deleteStudentAppointmentDetails(String RId, String SId)
    {
        String message = "Failure";
        try
        {
            conn = new SqlConnection(conStr);
            string deleteStudentAppointmentDetails = "delete from dbo.StudentAppointmentDetails where SId='" + SId +
                                                     "' and RId='" + RId + "'";
            SqlCommand command = new SqlCommand(deleteStudentAppointmentDetails, conn);
            conn.Open();
            int count = command.ExecuteNonQuery();

            if (count > 0)
            {
                message = "Success";
                conn.Close();
                return message;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: deleteStudentAppointmentDetails()", ex);
            conn.Close();
            return message;
        }
    }

    /*****************************************************************************************************************/
    //User Details

    //get User Details
    public void getUserDetails(String UId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentAppointmentHistory =
                "select RId, AppointmentDate from dbo.StudentAppointmentDetails where SId='" + UId + "'";
            SqlCommand command = new SqlCommand(getStudentAppointmentHistory, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
            else
            {
                throw new Exception();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentAppointmentHistory()", ex);
            conn.Close();
        }

        return dt;
    }

} //end of class