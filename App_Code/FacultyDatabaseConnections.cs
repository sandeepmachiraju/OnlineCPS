﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: FacultyDatabaseConnections.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for FacultyDatabaseConnections
/// </summary>
public class FacultyDatabaseConnections
{
    SqlConnection conn;
    string conStr = ConfigurationManager.ConnectionStrings["OnlineCPS"].ConnectionString;
    public FacultyDatabaseConnections()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /********************************************************************************************/
    // get Student Id and Request Id andd details of Request

    public DataTable getStudentRequests(string FId)
    {
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(conStr);
            string getStudentRequests =
                "select scrd.SId, scrd.RequestId,sld.SFirstName,scrd.RequestDate,sad.AppointmentDate, scrd.RequestStatus from dbo.StudentCpsRequestDetails scrd, dbo.StudentAppointmentDetails sad, dbo.StudentLoginDetails sld where scrd.SId= (select SId from dbo.StudentFaculty where FId='" + FId+"') and scrd.SId = sld.SId and scrd.RequestId= sad.RId";
            SqlCommand command = new SqlCommand(getStudentRequests, conn);
            conn.Open();

            dt.Load(command.ExecuteReader());
            if (dt != null)
            {
                conn.Close();
                return dt;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception in Connecting to OnlineCPS database::: getStudentRequests()", ex);
            conn.Close();
        }

        return dt;
    }
}