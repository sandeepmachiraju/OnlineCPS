﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: Startup.Auth.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace OnlineCPS
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301883
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and also store information about a user logging in with a third party login provider.
            // This is required if your application allows users to login
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}