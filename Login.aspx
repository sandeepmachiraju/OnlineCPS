﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="form">
  <div class="container">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-md-8 col-md-offset-2">
       <form runat="server">
          <div class="form-group">
                <div class="radio">
                <span style="padding-left:30px" />
                 <asp:RadioButton ID="Student" Text="Student" GroupName="Users" runat="server" Checked="true"/>
                   <img src="Images/Student.png" style="height:50px;width:50px"/> <span style="padding-left:20px" />
                 <asp:RadioButton ID="FacultyAdvisor" Text="Faculty Advisor" GroupName="Users" runat="server"/>
                  <img src="Images/Faculty.png" style="height:50px;width:50px"/>  <span style="padding-left:20px" />
                 <asp:RadioButton ID="AcademicAdvisor" Text="Academic Advisor" GroupName="Users" runat="server"/>
                <img src="Images/Academic.png" style="height:50px;width:50px"/></div>
              </div>                          
              <div class="form-group">
                <label>Id<span class="req">*</span> </label>
                <input type="text" class="form-control" id="identificationno" required data-validation-required-message="Please enter your Identification Number." autocomplete="off" runat="server"/>
                <p class="help-block text-danger"></p>
        
              </div>
              <div class="form-group">
                <label> Password<span class="req">*</span> </label>
                <input type="password" class="form-control" id="password" required data-validation-required-message="Please enter your password" autocomplete="off" runat="server">
                <p class="help-block text-danger"></p>
              </div>
              <div class="mrgn-30-top">
                <asp:button class="btn btn-larger btn-block" onclick="Login_Click" runat="server" Text="Login">
                </asp:button>
              </div>
           <span style="color:#FF9713; font-family: arial;font-size: 20px;"><asp:Label id="ErrorLabel" runat="server" /></span>
            </form>
          </div>
         
    </div>
  </div>
  <!-- /.container --> 


</asp:Content>


