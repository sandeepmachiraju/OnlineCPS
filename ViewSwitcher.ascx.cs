﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: ViewSwitcher.ascx.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:04 PM
// //   Description: 
// //-----------------------------------------------------------------------

using System;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls.Resolvers;

public partial class ViewSwitcher : System.Web.UI.UserControl
{
    protected string CurrentView { get; private set; }

    protected string AlternateView { get; private set; }

    protected string SwitchUrl { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Determine current view
        var isMobile = WebFormsFriendlyUrlResolver.IsMobileView(new HttpContextWrapper(Context));
        CurrentView = isMobile ? "Mobile" : "Desktop";

        // Determine alternate view
        AlternateView = isMobile ? "Desktop" : "Mobile";

        // Create switch URL from the route, e.g. ~/__FriendlyUrls_SwitchView/Mobile?ReturnUrl=/Page
        var switchViewRouteName = "AspNet.FriendlyUrls.SwitchView";
        var switchViewRoute = RouteTable.Routes[switchViewRouteName];
        if (switchViewRoute == null)
        {
            // Friendly URLs is not enabled or the name of the switch view route is out of sync
            this.Visible = false;
            return;
        }
        var url = GetRouteUrl(switchViewRouteName, new {view = AlternateView, __FriendlyUrls_SwitchViews = true});
        url += "?ReturnUrl=" + HttpUtility.UrlEncode(Request.RawUrl);
        SwitchUrl = url;
    }
}