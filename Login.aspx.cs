﻿// //-----------------------------------------------------------------------
// // <copyright file="ArcDefault.aspx.cs" company="Dream Team">
// //     2017 University of Houston Clear Lake, Capstone. All rights reserved.
// // </copyright>
// // <author> Sandeep Mittapally, Raja Anavema Reddy, Srinivasa Sai Sandeep Machiraju 
// // </author>
// //  Project Name: OnlineCPS
// //  File Name: Login.aspx.cs
// //  Created Date and Time: 05/01/2017, 6:19 PM
// //   Date and Time: 05/01/2017, 8:00 PM
// //   Description: 
// //-----------------------------------------------------------------------

using System;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ErrorLabel.Text = " ";
        // To check whether session exists or not;

        if (Session["LoggedUserId"] == null && !Request.Path.EndsWith("Login"))
        {
            Response.Redirect("../Login.aspx");
        }
        else if (Session["FacultyLoggedUserId"] == null && !Request.Path.EndsWith("Login"))
        {
            Response.Redirect("../Login.aspx");
        }
    }

    public void Login_Click(Object sender,
        EventArgs e)
    {
        StudentDatabaseConnections sdc = new StudentDatabaseConnections();
        String IdentificationNo = identificationno.Value.ToString();
        String Password = password.Value.ToString();
        String User = "None";
        if (Student.Checked == true)
        {
            User = "Student";
        }
        else if (FacultyAdvisor.Checked == true)
        {
            User = "FacultyAdvisor";
        }
        else if (AcademicAdvisor.Checked == true)
        {
            User = "AcademicAdvisor";
        }
        String LoginStatus = sdc.getLoginStatus(IdentificationNo, Password, User);
        if (LoginStatus == "Success")
        {
            if (User == "Student")
            {
                Session["LoggedUserId"] = IdentificationNo;
                Session["FacultyLoggedUserId"] = null;
                Response.Redirect("~/Student/StudentDefault.aspx");
            }
            else if (User == "FacultyAdvisor")
            {
                Session["FacultyLoggedUserId"] = IdentificationNo;
                Session["LoggedUserId"] = IdentificationNo;
                Response.Redirect("~/FacultyAdvisor/StudentRequests.aspx");
            }
        }
        else
        {
            ErrorLabel.Text = LoginStatus;
        }
    }
}